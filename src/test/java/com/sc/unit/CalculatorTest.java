package com.sc.unit;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {
    private Calculator calculator;

    @Before
    public void setUp() throws Exception {
        calculator = new Calculator();
    }

    @Test
    public void twoAndThreeIsFive() throws Exception {
        int first = 2;
        int second = 3;

        final long result = calculator.add(first, second);

        assertEquals(5L, result);
    }

    @Test
    public void twoAndZeroIsTwo() throws Exception {
        final long result = calculator.add(2, 0);

        assertEquals(2L, result);
    }

    @Test
    public void twoAndMinusTwoIsZero() throws Exception {
        final long result = calculator.add(2, -2);

        assertEquals(0L, result);
    }

    @Test
    public void threeMinusTwoIsOne() throws Exception {
        final long result = calculator.subtract(3, 2);

        assertEquals(1L, result);
    }

    @Test
    public void threeMinusThreeIsZero() throws Exception {
        final long result = calculator.subtract(3, 3);

        assertEquals(0L, result);
    }

    @Test
    public void threeMinusMinusThreeIsSix() throws Exception {
        final long result = calculator.subtract(3, -3);

        assertEquals(6L, result);
    }

    @Test
    public void threeXThreeIsNine() throws Exception {
        final long result = calculator.multiply(3, 3);

        assertEquals(9L, result);
    }

    @Test
    public void threeXZeroIsZero() throws Exception {
        final long result = calculator.multiply(3, 0);

        assertEquals(0L, result);
    }

    @Test
    public void threeXMinusThreeIsMinusNine() throws Exception {
        final long result = calculator.multiply(3, -3);

        assertEquals(-9L, result);
    }


}