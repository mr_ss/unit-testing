package com.sc.unit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class HttpServiceTest {
    @Mock
    private CloseableHttpClient client;

    @Mock
    private HttpRequestBase method;

    @Mock
    private CloseableHttpResponse response;

    @Mock
    private StatusLine statusLine;

    @Mock
    private HttpEntity httpEntity;

    private HttpService httpService;

    @Before
    public void setUp() {
        this.httpService = new HttpService(client);
    }

    @Test
    public void checkSimpleExecuteWithoutAnything() throws Exception {
        when(client.execute(method)).thenReturn(response);

        CloseableHttpResponse httpResponse = httpService.execute(method);

        assertThat("Looks like return response from another client!", httpResponse, is(equalTo(response)));
    }

    @Test(expected = IOException.class)
    public void checkThrowAwayIOException() throws Exception {
        when(client.execute(method)).thenThrow(IOException.class);

        httpService.execute(method);
    }

    @Test
    public void checkExecutionWithoutAnyClose() throws Exception {
        when(client.execute(method)).thenReturn(response);

        httpService.execute(method);

        verify(method, never()).releaseConnection();
        verify(response, never()).close();
    }

    @Test
    public void checkCorrectCloseAfterDoRequest() throws Exception {
        prepareOkRequestResponse();
        when(response.getEntity()).thenReturn(httpEntity);

        httpService.doRequest(method);

        verify(method).releaseConnection();
        verify(response).close();
    }

    @Test
    public void checkIOInDoRequest() throws Exception {
        when(client.execute(method)).thenThrow(IOException.class);

        HttpResult httpResult = httpService.doRequest(method);

        assertThat("Result must be error if any exception occurred", isErrorResponse(httpResult), is(equalTo(true)));
    }

    private boolean isErrorResponse(HttpResult result) {
        return -1 == result.getStatusCode();
    }

    @Test
    public void checkCloseAllIfAnyExceptionOccurred() throws Exception {
        prepareOkRequestResponse();
        when(response.getEntity()).thenReturn(null);

        httpService.doRequest(method);

        verify(method).releaseConnection();
        verify(response).close();
    }

    private void prepareOkRequestResponse() throws IOException {
        when(client.execute(method)).thenReturn(response);
        when(response.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(HttpStatus.SC_OK);
    }
}