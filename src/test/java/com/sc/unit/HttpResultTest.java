package com.sc.unit;

import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.message.BasicHeader;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

public class HttpResultTest {
    public static final String CREATED_ID = "CREATED_ID";

    @Test
    public void checkHeadersNotEmptyByDefault() throws Exception {
        HttpResult httpResult = new HttpResult(HttpStatus.SC_CREATED, CREATED_ID);

        List<Header> headers = httpResult.getHeaders();

        assertThat("Headers by default not null!", headers, is(notNullValue()));
    }

    @Test
    public void checkHeadersStoreWithoutChanges() throws Exception {
        List<Header> headers = new ArrayList<>();
        Header header = new BasicHeader("KEY", "VALUE");
        headers.add(header);
        headers.add(header);

        HttpResult httpResult = new HttpResult(HttpStatus.SC_CREATED, CREATED_ID, headers);

        assertThat("Look like headers changed after save in result!", httpResult.getHeaders(), is(hasSize(2)));
    }
}