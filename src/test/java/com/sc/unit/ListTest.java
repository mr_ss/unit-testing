package com.sc.unit;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ListTest {
    private List<String> list = new ArrayList<>();

    @Test
    public void testAdd() throws Exception {
        list.add("Foo");

        Assert.assertEquals(1, list.size());
    }

    @Test
    public void testAddTwoElements() throws Exception {
        list.add("Foo1");
        list.add("Foo2");

        Assert.assertEquals(2, list.size());
    }
}
