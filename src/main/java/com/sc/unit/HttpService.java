package com.sc.unit;

import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.Arrays;

public class HttpService {
    private final CloseableHttpClient client;

    public HttpService(CloseableHttpClient client) {
        this.client = client;
    }

    public CloseableHttpResponse execute(HttpRequestBase method) throws IOException {
        try {
            return this.client.execute(method);
        } catch (IOException e) {
            throw e;
        }
    }

    public HttpResult doRequest(HttpRequestBase method) {
        try(CloseableHttpResponse response = execute(method)) {
            int statusCode = response.getStatusLine().getStatusCode();
            String body = null;
            if(HttpStatus.SC_NO_CONTENT != statusCode) {
                body = EntityUtils.toString(response.getEntity());
            }
            Header[] allHeaders = response.getAllHeaders();
            return new HttpResult(statusCode, body, Arrays.asList(allHeaders));
        } catch (Exception e) {
            return new HttpResult(-1, e.getMessage());
        } finally {
            method.releaseConnection();
        }
    }
}
